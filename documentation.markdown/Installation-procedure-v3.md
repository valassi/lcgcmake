This file describes how to install generators to MCGenerators trees:

1. Download new authors tarfile to local source tarfile repository:

    a. Obtain sftnight AFS token;

    b. Download source tarball into `/afs/cern.ch/sw/lcg/external/tarFiles/MCGeneratorsTarFiles`;

2. Add package definition to requested version of LCGCMT release: `cmake/toolchain/heptools-<release>.cmake`;

3. Try build on local node and run GENSER test for the given package;

4. Commit changes into git;

5. For LCG releases before 67c:

        cmake -DLCG_INSTALL_PREFIX=/afs/cern.ch/sw/lcg/external/ -DLCG_VERSION=<RELEASE> -DLCG_TARBALL_INSTALL=ON -DCMAKE_INSTALL_PREFIX=/afs/.cern.ch/sw/lcg/external/ -DLCG_SAFE_INSTALL=ON ../lcgcmake

    Message `Unrecognized switch: -E  (-h will show valid options)` at post-install step is to be ignored.

    You will need to do update AFS replica after this (`afs_admin vos_release /afs/.cern.ch/sw/lcg/external/MCGenerators_lcgcmt<RELEASE>`)

    For LCG release 67c:

        cmake -DLCG_INSTALL_PREFIX=/afs/cern.ch/sw/lcg/external/ -DLCG_VERSION=67c -DLCG_TARBALL_INSTALL=ON -DCMAKE_INSTALL_PREFIX=/afs/cern.ch/sw/lcg/external/ -DLCG_SAFE_INSTALL=ON ../lcgcmake
   
    FOR THE LCG TREES 68 and higher: continue as below. (:warning: LCG toolchains dev2, dev3, dev4 and experimental are build automatically! :warning:)

6. Go to https://phsft-jenkins.cern.ch/job/lcg_release/
   or to the main Jenkins page https://phsft-jenkins.cern.ch/ and select lcg_release
   This requires permission (now it is done through the GENSER e-group)

7. Login to Jenkins. This requires a Jenkins account, to be created once (ask Patricia).

8. Select "Build with parameters" and build the package requested:

    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG release version (e.g. 71, 71root6, ...) |
    | TARGET       | name of the top level package (e.g. `rivet-2.3.0`) |
    | BUILD_POLICY | Generators |
    | LCG_INSTALL_PREFIX | /afs/cern.ch/sw/lcg/releases |
    |              | /afs/cern.ch/sw/lcg/releases/LCG_79 if building a package in LCG_79 that depends on pytools/pyanalysis |
    | Combinations | select the necessary generators for "slc6-physical". | 
    | VERSION      | LCGCMake branch to use for the build (releases_below_79 or LCG_<version>) | 


    NOTES: 

    a. as of 17.03.2015 there is no Jenkins procedure to substitute a generator version already installed (to repair it for example). Such generator versions are now ignored (rpms not copied or not created)

    b. For releases 81b, 81c only select gcc49 - these were not built with gcc48. None of the releases (at the moment of writing) require gcc51.

9. Go to https://phsft-jenkins.cern.ch/job/lcg_release_installToAFS/
    or to the main Jenkins page https://phsft-jenkins.cern.ch/ and select lcg_release_installToAFS       

10. Select "Build with parameters" and build it:

    | Parameter    | Value |
    | ------------ | -------------------------------------------- |
    | LCG_VERSION  | LCG version to install |
    | TARGET       | Target to install ("all" for entire release or "generators" for installing new generators) |
    | RPM_REVISION | "auto", unless instructed otherwise |

11. If the installation is successfull, mail to <mailto:genser-announce@cern.ch> like this:
    > Dear colleagues,
    
    > New Tauola++ version - Tauola++ 1.1.4 has been installed into MCGenerators lcgcmt trees:

    >  \- MCGenerators_lcgcmt65

    >   \- MCGenerators_lcgcmt65a

    >   \- MCGenerators_lcgcmt66

    >   \---

    >   Best regards,

    >   GENSER
